# UM User List #
**Contributors:**      SuitePlugins  
**Donate link:**       https://suiteplugins.com  
**Tags:**  
**Requires at least:** 4.4  
**Tested up to:**      4.8.1 
**Stable tag:**        1.0.0  
**License:**           GPLv3  
**License URI:**       http://www.gnu.org/licenses/gpl-2.0.html  

## Description ##

Neat plugin that lists Ultimate Member users in widget and shortcode

## Installation ##

### Manual Installation ###

1. Upload the entire `/um-user-list` directory to the `/wp-content/plugins/` directory.
2. Activate UM User List through the 'Plugins' menu in WordPress.

## Frequently Asked Questions ##


## Screenshots ##


## Changelog ##

### 1.0.0 ###
* First release

## Upgrade Notice ##

### 1.0.0 ###
First Release
